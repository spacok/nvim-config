local status_ok, alpha = pcall(require, "alpha")
local logo = require("user.logo")
local fortune = require("user.plugins.fortune")
fortune.setup({fortunes = {"futurama"}})
if not status_ok then
	return
end
local dashboard = require("alpha.themes.dashboard")
local boxtext = require("user.plugins.boxtext")
local plugins = ""
local date = os.date("%A, %d %B %Y")
local handle
local env_OS = os.getenv("OS")
if (env_OS == nil or env_OS:find("Windows") == nil) then
    handle = io.popen 'ls $HOME"/.local/share/nvim/site/pack/packer/start" | wc -l'
    plugins = handle:read "*a"
    plugins = plugins:gsub('\n','')
    handle:close()
elseif (env_OS:find("Windows") ~= nil) then
    handle = io.popen 'dir %LOCALAPPDATA%\\nvim-data\\site\\pack\\packer\\start /b /ad'
    local files = handle:read "*a"
    plugins = tostring(select(2, files:gsub('\n', '\n')))
end

dashboard.section.header.val = logo.get()
dashboard.section.buttons.val = {
    dashboard.button("f", "  Find file", ":Telescope find_files <CR>"),
    dashboard.button("e", "  New file", ":ene <BAR> startinsert <CR>"),
    dashboard.button("p", "  Find project", ":Telescope projects <CR>"),
    dashboard.button("r", "  Recently used files", ":Telescope oldfiles <CR>"),
    dashboard.button("t", "  Find text", ":Telescope live_grep <CR>"),
}
if (env_OS == nil or env_OS:find("Windows") == nil) then
    table.insert(dashboard.section.buttons.val, dashboard.button("c", "  Configuration", ":e ~/.config/nvim/init.lua <CR>"))
elseif (env_OS:find("Windows") ~= nil) then
    table.insert(dashboard.section.buttons.val, dashboard.button("c", "  Configuration", ":e " .. os.getenv("LOCALAPPDATA") .. "\\nvim\\init.lua <CR>"))
end

table.insert(dashboard.section.buttons.val, dashboard.button("q", "  Quit Neovim", ":qa<CR>"))

local function footer()
    boxtext.setup()
    local strings={
        {
            icon = "  ",
            text = fortune.string(),
        },
        {
            icon = "  ",
            text = "Today is " .. date,
        },
        {
            icon = "  ",
            text = plugins .. " plugins in total",
        }
    }
	return boxtext.make_strings(strings)
end
dashboard.section.footer.val = footer()

--[[ dashboard.section.footer.opts.hl = "Type"
dashboard.section.header.opts.hl = "Include"
dashboard.section.buttons.opts.hl = "Keyword" ]]

dashboard.opts.opts.noautocmd = true
-- vim.cmd([[autocmd User AlphaReady echo 'ready']])
alpha.setup(dashboard.config)
