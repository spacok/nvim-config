local status_ok, devicons  = pcall(require, "nvim-web-devicons")
if not status_ok then
  return
end
devicons.setup {
    override = {
        ["folder"] = {
            icon = "",
            color = "#f69a1b",
            cterm_color = "208",
            name = "Folder",
        },
    }
}

