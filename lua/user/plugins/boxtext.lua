local M ={}
local config = {}


local function align_string(icon, str, is_start, is_end)
    local tmpstr = str or ""
    while tmpstr:len() < config.width - config.iconheight - 6 do
            tmpstr = tmpstr .. " "
    end
    local prefix = "│  "
    local suffix = "  │"
    if is_start then
        prefix = "┌─ "
        suffix = " ─┐"
    elseif is_end then
        prefix = "└─ "
        suffix = " ─┘"
    end
    tmpstr = prefix .. icon .. tmpstr .. suffix
    return tmpstr
end

local function ltrim(s)
  return s:match'^%s*(.*)'
end

local function format_str(icon, inputstr, is_start, is_end)
    local t = {}
    local count = 0
    local tmplines = {}
    for str in string.gmatch(inputstr, "([^\n]+)") do
        table.insert(tmplines, ltrim(str))
    end
    for i, str in pairs(tmplines) do
        if str:len() >  config.width - config.iconheight - 6 then
            local tmpstr = ""
            local tmpt = {}
            for tmps in string.gmatch(str, "([^ ]+)") do
                table.insert(tmpt, tmps)
            end
            for v, split in pairs(tmpt) do
                local new = ""
                if tmpstr:len() > 0 then
                    new = tmpstr .. " " .. split
                else
                    new = split
                end
                if new:len() > config.width - config.iconheight - 6 then
                    local start = false
                    local useicon = "   "
                    if count == 0 then
                        start = is_start
                        count = count + 1
                        useicon = icon
                    end
                    tmpstr = align_string(useicon, tmpstr, start, false)
                    useicon = "   "
                    table.insert(t, tmpstr)
                    if v == #tmpt then
                        local isend = false
                        if i == #tmplines then
                            isend = is_end
                        end
                        tmpstr = align_string(useicon, split, false, isend)
                        table.insert(t, tmpstr)
                    end
                    new = split
                elseif v == #tmpt then
                    local useicon = "   "
                    local isend = false
                    if count == 0 then
                        count = count + 1
                        useicon = icon
                    end
                    if i == #tmplines then
                        isend = is_end
                    end
                    tmpstr = align_string(useicon, new, false, isend)
                    table.insert(t, tmpstr)
                    new = split
                end
                tmpstr = new
            end
        else
            local useicon = "   "
            local isstart = false
            local isend = false
            if count == 0 then
                useicon = icon
                count = count + 1
                isstart = is_start
            end
            if i == #tmplines then
                isend = is_end
                if isend then
                    isstart = false
                end
            end
            str = align_string(useicon, str, isstart, isend )
            table.insert(t, str)
        end
    end
    return t
end

function M.make_strings(strs)
    if not strs then
        return {}
    end
    local t = {}
    for v, k in pairs(strs) do
        local is_start = false
        local is_end = false
        if v == 1 then
            is_start = true
        end
        if v == #strs then
            is_end = true
        end
        local tmp = format_str(k.icon, k.text, is_start, is_end)
        for _, str in pairs(tmp) do
            table.insert(t,str)
        end
        if not is_end and config.add_empty then
            local tmp2 = align_string("   ", " ", false, false)
            table.insert(t,tmp2)
        end
    end
    return t
end

function M.setup(opts)
    opts = opts or {}
    config = {
        width = opts.witdh or 60,
        iconheight = 3,
        add_empty = true,
    }

end

return M
