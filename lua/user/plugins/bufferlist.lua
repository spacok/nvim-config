
local status_ok, _ = pcall(require, "telescope")
if not status_ok then
  return
end
local pickers = require "telescope.pickers"
local finders = require "telescope.finders"
local conf = require("telescope.config").values
local transform_mod = require('telescope.actions.mt').transform_mod
local actions = require "telescope.actions"

-- or create your custom action
local close_buffer= transform_mod({
  x = function(prompt_bufnr)
    print("This function ran after another action. Prompt_bufnr: " .. prompt_bufnr)
    -- Enter your function logic here. You can take inspiration from lua/telescope/actions.lua
  end,
})
local utils = require "telescope.utils"
local strings = require "plenary.strings"
local entry_display = require "telescope.pickers.entry_display"
local Path = require "plenary.path"
local M={}

local function gen_from_buffer(opts)
    opts = opts or {}

    local disable_devicons = opts.disable_devicons

    local icon_width = 0
        if not disable_devicons then
        local icon, _ = utils.get_devicons("fname", disable_devicons)
        icon_width = strings.strdisplaywidth(icon)
    end

    local displayer = entry_display.create {
        separator = " ",
        items = {
            { width = opts.bufnr_width },
            { width = 4 },
            { width = icon_width },
            { width = math.floor(vim.api.nvim_win_get_width(0) * 0.22)},
            { width = icon_width },
            { remaining = true },
        },
    }

    local make_display = function(entry)
        local display_bufname = utils.transform_path(opts, entry.filename)

        local icon, hl_group = utils.get_devicons(entry.filename, disable_devicons)
        local has_devicons, devicons = pcall(require, "nvim-web-devicons")
        local foldericon = ""
        local folderhl_group = ""
        if (has_devicons) then
            foldericon, folderhl_group = devicons.get_icon("folder", "folder", { default = true })
        end

        return displayer {
            { entry.bufnr, "TelescopeResultsNumber" },
            { entry.indicator, "TelescopeResultsComment" },
            { icon, hl_group },
            { display_bufname .. ":" .. entry.lnum },
            { foldericon, folderhl_group},
            { entry.path},
        }
    end

    local path_file = function (str, sep)
        if (sep) then
            local revstr = str:reverse()
            local ind = revstr:find(sep)
            if ind then
                return str:sub(#revstr - ind + 2), str:sub(1, #revstr - ind )
            end
        end
        return str, ""
    end

    return function(entry)
        local bufname = entry.info.name ~= "" and entry.info.name or "[No Name]"
        local sep = Path:new(bufname):parent()._sep
        local bufcwd
        bufname, bufcwd = path_file(bufname, sep)
        local hidden = entry.info.hidden == 1 and "h" or "a"
        local readonly = vim.api.nvim_buf_get_option(entry.bufnr, "readonly") and "=" or " "
        local changed = entry.info.changed == 1 and "+" or " "
        local indicator = entry.flag .. hidden .. readonly .. changed
        local line_count = vim.api.nvim_buf_line_count(entry.bufnr)

        return {
            valid = true,
            value = bufname,
            ordinal = entry.bufnr .. " : " .. bufname,
            display = make_display,
            bufnr = entry.bufnr,
            filename = bufname,
            -- account for potentially stale lnum as getbufinfo might not be updated or from resuming buffers picker
            lnum = entry.info.lnum ~= 0 and math.max(math.min(entry.info.lnum, line_count), 1) or 1,
            path = bufcwd,
            indicator = indicator,
        }
    end
end

function M.get_dropdown(opts)
    opts = opts or {}
    opts.preview = false
    opts.sort_lastused = true
    opts.entry_maker = function(a) gen_from_buffer(a) end
    local theme_opts = {
        theme = "dropdown",
        results_title = false,
        sorting_strategy = "ascending",
        layout_strategy = "center",
        layout_config = {
            preview_cutoff = 1, -- Preview should always show (unless previewer = false)
            prompt_position = "bottom",
            width = function(_, max_columns, _)
                    return math.min(max_columns, 220)
                end,
            height = function(_, _, max_lines)
                    return math.min(max_lines, 25)
                end,
        },
        border = true,
        borderchars = {
          prompt = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
          results = { "─", "│", "─", "│", "┌", "┐", "┤", "├" },
          preview = { "─", "│", "─", "│", "┌", "┐", "┘", "└" },
        },
    }
    return vim.tbl_deep_extend("force", theme_opts, opts)
end

function M.makebuffers()
    local opts = M.get_dropdown()
    local has_cwd_only = opts.cwd_only ~= nil
    local has_only_cwd = opts.only_cwd ~= nil

    if has_only_cwd and not has_cwd_only then
        -- Internally, use cwd_only
        opts.cwd_only = opts.only_cwd
        opts.only_cwd = nil
    end
    local bufnrs = vim.tbl_filter(function(b)
        if 1 ~= vim.fn.buflisted(b) then
          return false
        end
        -- only hide unloaded buffers if opts.show_all_buffers is false, keep them listed if true or nil
        if opts.show_all_buffers == false and not vim.api.nvim_buf_is_loaded(b) then
          return false
        end
        if opts.ignore_current_buffer and b == vim.api.nvim_get_current_buf() then
          return false
        end
        if opts.cwd_only and not string.find(vim.api.nvim_buf_get_name(b), vim.loop.cwd(), 1, true) then
          return false
        end
        return true
    end, vim.api.nvim_list_bufs())
    if not next(bufnrs) then
        return
      end
    if opts.sort_mru then
        table.sort(bufnrs, function(a, b)
            return vim.fn.getbufinfo(a)[1].lastused > vim.fn.getbufinfo(b)[1].lastused
        end)
    end

    local buffers = {}
    local default_selection_idx = 1
    for _, bufnr in ipairs(bufnrs) do
        local flag = bufnr == vim.fn.bufnr "" and "%" or (bufnr == vim.fn.bufnr "#" and "#" or " ")

        if opts.sort_lastused and not opts.ignore_current_buffer and flag == "#" then
            default_selection_idx = 2
        end

        local element = {
            bufnr = bufnr,
            flag = flag,
            info = vim.fn.getbufinfo(bufnr)[1],
        }

        if opts.sort_lastused and (flag == "#" or flag == "%") then
            local idx = ((buffers[1] ~= nil and buffers[1].flag == "%") and 2 or 1)
            table.insert(buffers, idx, element)
        else
            table.insert(buffers, element)
        end
    end
    if not opts.bufnr_width then
        local max_bufnr = math.max(unpack(bufnrs))
        opts.bufnr_width = #tostring(max_bufnr)
    end

    pickers.new(opts, {
        prompt_title = "Buffers",
        finder = finders.new_table {
            results = buffers,
            entry_maker = gen_from_buffer(opts),
        },
        previewer = nil;
        sorter = conf.generic_sorter(opts),
        default_selection_index = default_selection_idx,
    }):find()
end

return M
