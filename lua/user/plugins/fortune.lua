local errmsg = ""
local config = {}
local ends = {".","!","?", "\""}
local function is_win()
    local env_OS = os.getenv("OS")
    if (env_OS and env_OS:find("Windows") ~= nil) then
        return true
    end
    return false
end

local function get_fortunes(path)
    if path == nil then
        return {}
    end
    local cmd = ""
    if not is_win() then
        cmd = 'ls -l ' .. path
    else
        cmd = 'dir ' .. path .. ' /b /ad'
    end
    local handle = io.popen(cmd)
    local t = handle:read("*a"):gsub('\n', '\n')
    local fortunes = {}
    for _, k in pairs(t) do
        if not k:find(".") then
            table.insert(fortunes,k)
        end
    end
    return fortunes
end

local function trim(s)
    return (string.gsub(s, "^%s*(.-)%s*$", "%1"))
end

local function fortune_path()
    if not config.fortunedir then
        config.fortunedir = os.getenv("FORTUNEDIR")
    end
    if not config.fortunedir and is_win() then
        errmsg = "Missing %FORTUNEDIR% or opts.fortunedir variable."
        return nil
    elseif not config.fortunedir then
        config.fortunedir = "/usr/share/fortune/"
    end
    config.fortunedir = config.fortunedir:gsub("^\\","/")
    return config.fortunedir
end

local function joinstrings (str1, str2)
    local joinchar = " "
    local tmp1 = trim(str1)
    local tmp2 = trim(str2)
    for _, val in pairs(ends) do
        if (string.sub(tmp1, -1) == val) or
           (string.sub(tmp2, 0, 1) == val) then
            joinchar = "\n"
        end
    end
    return tmp1 .. joinchar .. tmp2
end

local function exists(file)
    local ok, _, code = os.rename(file, file)
    if not ok then
        if code == 13 or code == 2 then
            -- Permission denied, but it exists
            return true
        end
    end
    return ok
end

--- Check if a directory exists in this path
local function isdir(path)
    -- "/" works on both Unix and Windows
    if (path:sub(-1) ~= "/") then
        path = path .. '/'
    end
    return exists(path)
end

local function element_to_string(item)
    local str = ""
    for _, f in pairs(item) do
        str = joinstrings(str,f)
    end
    return str
end

local function get_string(filename)
    local strings = {}
    local element = {}
    for line in io.lines(config.fortunedir .. filename) do
        if line:sub(1) == '%' then
            table.insert(strings,element)
            element={}
        else
            table.insert(element,line)
        end
    end
    if element then
        table.insert(strings,element)
    end
    return element_to_string(strings[math.random(#strings)])
end

local M = {}

function M.setup(opts)
    config.fortunedir = opts.fortunedir or fortune_path()
    config.fortunes = opts.fortunes or get_fortunes(config.fortunedir)

end

function M.string()
    if not config.fortunedir then
        return {errmsg}
    end
    if not isdir(config.fortunedir) then
        return {config.fortunedir .. " does not exists"}
    end
    math.randomseed(os.time())
    local filename = config.fortunes[math.random(#config.fortunes)]
    if (exists(filename)) then
        return get_string(filename)
    else
        return (config.fortunedir .. filename .. " does not exists")
    end
end

return M
