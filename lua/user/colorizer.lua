local clr_status_ok, colorizer = pcall(require, "colorizer")
if not clr_status_ok then
  return
end
colorizer.setup()

