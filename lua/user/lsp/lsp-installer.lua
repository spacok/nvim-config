local status_ok, mason = pcall(require, "mason")
status_ok, masonlspconfig = pcall(require, "mason-lspconfig")
if not status_ok then
	return
end

-- Register a handler that will be called for all installed servers.
-- Alternatively, you may also register handlers on specific server instances instead (see example below).
mason.setup({
    ui = {
        icons = {
            package_installed = "✓",
            package_pending = "➜",
            package_uninstalled = "✗"
        }
    }
})
masonlspconfig.setup()

